<?php

/**
 * @file
 * Rules integration for amoCRM.
 */

/**
 * Implements hook_rules_action_info().
 *
 * @ingroup rules
 */
function amocrm_rules_action_info() {
  $actions = array();
  try {
    if (!($client = amocrm_api_get_default_client())) {
      throw new Exception(t('Cannot initialize amoCRM client. Rules actions will not be defined.'));
    }
    $client_info = amocrm_api_account_load($client);

    // Define actions for contacts, leads and companies.
    $entities = array(
      'contacts' => 'contact',
      'leads' => 'lead',
      'companies' => 'company',
    );

    foreach ($entities as $machine_name => $name) {
      $variables = array();
      // Get all available custom fields for the current entity.
      if (!empty($client_info['custom_fields'][$machine_name])) {
        foreach ($client_info['custom_fields'][$machine_name] as $entity_custom_field) {
          if (empty($entity_custom_field['enums'])) {
            $variables[$entity_custom_field['id']] = array(
              'label' => $entity_custom_field['name'],
              'type' => 'text',
              'optional' => TRUE,
            );
          }
          // To get all add-on fields with the key "enum".
          else {
            foreach ($entity_custom_field['enums'] as $enum_key => $enum_name) {
              $variables[$entity_custom_field['id'] . '_' . $enum_key] = array(
                'label' => t('!field_name - (!enum_name)', array(
                  '!enum_name' => $enum_name,
                  '!field_name' => $entity_custom_field['name'],
                )),
                'type' => 'text',
                'optional' => TRUE,
              );
            }
          }
        }
      }
      if ($machine_name == 'leads') {
        foreach ($client_info['leads_statuses'] as $status) {
          $variables['leads_statuses'][$status['id']] = $status['name'];
        }
      }
      $actions += _amocrm_build_actions($variables, $machine_name, $name);
    }
    // Add other actions for notes and tasks.
    $actions += _amocrm_build_actions_notes();
    $actions += _amocrm_build_actions_tasks($client_info['task_types']);
  }
  catch (Exception $e) {
    watchdog_exception('amoCRM', $e);
  }

  return $actions;
}

/**
 * Implements hook_rules_event_info().
 */
function amocrm_rules_event_info() {
  $types = amocrm_get_amocrm_object_types();

  $vars[AMOCRM_TYPE_LEAD] = array(
    'amocrm' => array(
      'type' => 'amocrm_' . AMOCRM_TYPE_LEAD,
      'label' => t('Input leads data from amoCRM webhook.'),
      'description' => t('When lead is deleted, there is no available tokens, except lead ID.'),
    ),
  );
  $vars[AMOCRM_TYPE_CONTACT] = array(
    'amocrm' => array(
      'type' => 'amocrm_' . AMOCRM_TYPE_CONTACT,
      'label' => t('Input contacts data from amoCRM webhook.'),
      'description' => t('When contact is deleted, there is no available tokens, except contact ID.'),
    ),
  );
  $vars[AMOCRM_TYPE_COMPANY] = array(
    'amocrm' => array(
      'type' => 'amocrm_' . AMOCRM_TYPE_COMPANY,
      'label' => t('Input companies data from amoCRM webhook.'),
      'description' => t('When company is deleted, there is no available tokens, except company ID.'),
    ),
  );

  foreach ($types as $type) {
    $items['amocrm_' . $type . '_' . AMOCRM_CRUD_OPERATIONS_CREATE] = array(
      'label' => t('After saving new !type', array('!type' => $type)),
      'group' => t('amoCRM'),
      'variables' => $vars[$type],
    );
    $items['amocrm_' . $type . '_' . AMOCRM_CRUD_OPERATIONS_UPDATE] = array(
      'label' => t('After updating existing !type', array('!type' => $type)),
      'group' => t('amoCRM'),
      'variables' => $vars[$type],
    );
    $items['amocrm_' . $type . '_' . AMOCRM_CRUD_OPERATIONS_DELETE] = array(
      'label' => t('After deleting !type', array('!type' => $type)),
      'group' => t('amoCRM'),
      'variables' => $vars[$type],
    );

    if ($type == AMOCRM_TYPE_LEAD) {
      $items['amocrm_' . $type . '_' . AMOCRM_CRUD_OPERATIONS_STATUS] = array(
        'label' => t('After changing status of existing !type', array('!type' => $type)),
        'group' => t('amoCRM'),
        'variables' => $vars[$type],
      );
      $items['amocrm_' . $type . '_' . AMOCRM_CRUD_OPERATIONS_RESTORE] = array(
        'label' => t('After restoring deleted !type', array('!type' => $type)),
        'group' => t('amoCRM'),
        'variables' => $vars[$type],
      );
    }
  }

  return $items;
}

/**
 * Rules action.
 */
function _amocrm_entity_create($args, $element) {
  try {
    if (!($client = amocrm_api_get_default_client())) {
      throw new Exception(t('Cannot initialize amoCRM client. Rules actions will not be defined.'));
    }

    $client_info = amocrm_api_account_load($client);
    $entity_type = $element->getElementName();

    $info = array(
      'name' => !empty($args['name']) ? $args['name'] : '',
      'tags' => !empty($args['tags']) ? $args['tags'] : '',
      'date_create' => REQUEST_TIME,
      'last_modified' => REQUEST_TIME,
      'responsible_user_id' => !empty($args['responsible_user_id']) ? $args['responsible_user_id'] : '',
    );

    $info['custom_fields'] = _amocrm_prepare_info($entity_type, $client_info, $args);

    switch ($entity_type) {
      case 'contacts':
        amocrm_api_contacts_add($client, array($info));
        break;

      case 'companies':
        amocrm_api_companies_add($client, array($info));
        break;

      case 'leads':
        $info += array(
          'sale' => !empty($args['price']) ? $args['price'] : 0,
          'status_id' => !empty($args['status_id']) ? $args['status_id'] : 0,
          'contacts_id' => !empty($args['contacts_id']) ? explode(',', $args['contacts_id']) : array(),
        );
        amocrm_api_leads_add($client, array($info));
        break;
    }
  }
  catch (Exception $e) {
    watchdog_exception('amoCRM', $e);
  }
}

/**
 * Rules action.
 */
function _amocrm_entity_update($args, $element) {
  try {
    if (!($client = amocrm_api_get_default_client())) {
      throw new Exception(t('Cannot initialize amoCRM client. Rules actions will not be defined.'));
    }
    $client_info = amocrm_api_account_load($client);

    $action_name = $element->getElementName();
    $action_name = explode('_', $action_name);
    $entity_type = $action_name[0];

    $info = array(
      'name' => !empty($args['name']) ? $args['name'] : '',
      'tags' => !empty($args['tags']) ? $args['tags'] : '',
      'date_create' => REQUEST_TIME,
      'last_modified' => REQUEST_TIME,
      'responsible_user_id' => !empty($args['responsible_user_id']) ? $args['responsible_user_id'] : '',
      'id' => $args['entity_id'],
    );

    $info['custom_fields'] = _amocrm_prepare_info($entity_type, $client_info, $args);

    switch ($entity_type) {
      case 'contacts':
        amocrm_api_contacts_update($client, array($info));
        break;

      case 'companies':
        amocrm_api_companies_update($client, array($info));
        break;

      case 'leads':
        $info += array(
          'sale' => !empty($args['price']) ? $args['price'] : 0,
          'status_id' => !empty($args['status_id']) ? $args['status_id'] : 0,
          'contacts_id' => !empty($args['contacts_id']) ? explode(',', $args['contacts_id'])
            : array(),
        );
        amocrm_api_leads_update($client, array($info));
        break;
    }
  }
  catch (Exception $e) {
    watchdog_exception('amoCRM', $e);
  }
}

/**
 * Prepare info for amoCRM request.
 */
function _amocrm_prepare_info($type, $client_info, $params) {
  $custom_fields = array();
  if (!empty($client_info['custom_fields'][$type])) {
    foreach ($client_info['custom_fields'][$type] as $entity_custom_field) {
      if (!empty($entity_custom_field['enums'])) {
        foreach ($entity_custom_field['enums'] as $enum_key => $enum_name) {
          $value_key = $entity_custom_field['id'] . '_' . $enum_key;
          if (!empty($params[$value_key])) {
            $custom_fields[] = array(
              'id' => $entity_custom_field['id'],
              'values' => array(
                array(
                  'value' => $params[$value_key],
                  'enum' => $enum_key,
                ),
              ),
            );
          }
        }
      }
      else {
        if (!empty($params[$entity_custom_field['id']])) {
          $custom_fields[] = array(
            'id' => $entity_custom_field['id'],
            'values' => array(
              array(
                'value' => $params[$entity_custom_field['id']],
              ),
            ),
          );
        }
      }
    }
  }

  return $custom_fields;
}

/**
 * Get leads statuses.
 */
function _amocrm_lead_statuses_list() {
  $client = amocrm_api_get_default_client();
  $client_info = amocrm_api_account_load($client);
  $leads_statuses = array();

  if (!empty($client_info['leads_statuses'])) {
    foreach ($client_info['leads_statuses'] as $status) {
      $leads_statuses[$status['id']] = $status['name'];
    }
  }

  return $leads_statuses;
}

/**
 * Get leads statuses.
 */
function _amocrm_user_list() {
  $client = amocrm_api_get_default_client();
  $client_info = amocrm_api_account_load($client);
  $users = array();

  if (!empty($client_info['users'])) {
    foreach ($client_info['users'] as $user) {
      $users[$user['id']] = $user['name'] . ' ' . $user['last_name'];
    }
  }

  return $users;
}

/**
 * Building rules actions.
 */
function _amocrm_build_actions($variables, $machine_name, $human_name) {
  $action_info = array(
    'group' => t('amoCRM'),
    'named parameter' => TRUE,
    'parameter' => array(
      'responsible_user_id' => array(
        'label' => t('Responsible user'),
        'type' => 'integer',
        'optional' => TRUE,
        'options list' => '_amocrm_user_list',
      ),
      'tags' => array(
        'label' => t('Tags'),
        'description' => t('Use a comma to separate the tags.'),
        'type' => 'text',
        'optional' => TRUE,
      ),
      'name' => array(
        'label' => t('Name'),
        'type' => 'text',
      ),
    ),
  );

  if ($machine_name == 'leads') {
    $statuses = $variables['leads_statuses'];
    unset($variables['leads_statuses']);
  }

  foreach (array('create', 'update') as $op) {
    $name = ($op == 'create') ? $machine_name : "{$machine_name}_$op";

    $actions[$name] = $action_info;
    // Standard label for all the types and operations.
    $actions[$name]['label'] = t(
      '@action @name in amoCRM',
      array('@action' => t(drupal_ucfirst($op)), '@name' => t($human_name))
    );
    // Base callback function for current action.
    $actions[$name]['base'] = "_amocrm_entity_$op";

    // Add specific parameter for update operation.
    if ($op == 'update') {
      $actions[$name]['parameter']['entity_id'] = array(
        'label' => t('@name ID', array('@name' => drupal_ucfirst($human_name))),
        'type' => 'text',
      );
      $actions[$name]['parameter']['name']['optional'] = TRUE;
    }
    // Add custom fields to parameters list.
    $actions[$name]['parameter'] += $variables;
    // Small patch for a leads. We have to add statuses list for to the action
    // settings form.
    if ($machine_name == 'leads') {
      $actions[$name]['parameter']['price'] = array(
        'label' => t('Lead price'),
        'optional' => TRUE,
        'type' => 'integer',
      );
      $actions[$name]['parameter']['contacts_id'] = array(
        'label' => t('Contacts identificators'),
        'optional' => TRUE,
        'type' => 'text',
      );
      $actions[$name]['parameter']['status_id'] = array(
        'label' => t('Lead status'),
        'optional' => TRUE,
        'type' => 'integer',
        'statuses_list' => $statuses,
        'options list' => 'amocrm_rules_get_statuses_list',
      );
    }
  }

  return $actions;
}

/**
 * Provides statuses list for an options list.
 */
function amocrm_rules_get_statuses_list($element, $name) {
  $info = $element->info();
  if (!empty($info['parameter']['status_id']['statuses_list'])) {
    $options = array('- ' . t('None') . ' -');
    $options += $info['parameter']['status_id']['statuses_list'];
    return $options;
  }

  return array();
}

/**
 * Building note actions.
 */
function _amocrm_build_actions_notes() {
  foreach (array('add', 'update') as $action_type) {
    $name = "notes_$action_type";

    $actions[$name] = array(
      'group' => t('amoCRM'),
      'named parameter' => TRUE,
      'base' => 'amocrm_rules_notes_action',
      'label' => t(
        '@action @name in amoCRM',
        array('@action' => t('Create'), '@name' => t('note'))
      ),
      'parameter' => array(
        'responsible_user_id' => array(
          'label' => t('Responsible user'),
          'type' => 'integer',
          'optional' => TRUE,
          'options list' => '_amocrm_user_list',
        ),
        'element_id' => array(
          'label' => t('Element ID'),
          'description' => t('Contact or lead ID.'),
          'type' => 'integer',
        ),
        'element_type' => array(
          'label' => t('Element type'),
          'type' => 'integer',
          'options list' => '_amocrm_rules_related_entity_type_list',
        ),
        'text' => array(
          'label' => t('Text'),
          'type' => 'text',
        ),
      ),
    );
  }
  // Add another parameter for an update OP type.
  $actions['notes_update']['parameter']['note_id'] = array(
    'label' => t('Note ID'),
    'type' => 'integer',
  );

  return $actions;
}

/**
 * Rules action callback. Creates or updates note.
 */
function amocrm_rules_notes_action($args, $element) {
  $name = $element->getElementName();
  $op = strpos($name, 'update') === FALSE ? 'add' : 'update';

  // Info for both OPs.
  $info = array(
    'text' => token_replace($args['text']),
    'responsible_user_id' => !empty($args['responsible_user_id']) ? $args['responsible_user_id'] : NULL,
    'note_type' => AMOCRM_API_NOTE_COMMON,
    'element_type' => $args['element_type'],
    'element_id' => $args['element_id'],
  );

  // Add specific note ID for update operation.
  if ($op == 'update') {
    $info['id'] = $args['note_id'];
  }

  // Call update or add functions.
  $function = "amocrm_api_notes_$op";
  $function(amocrm_api_get_default_client(), array($info));
}

/**
 * Provides actions related amoCRM tasks.
 */
function _amocrm_build_actions_tasks($types) {
  foreach ($types as $info) {
    $task_types[$info['id']] = $info['name'];
  }

  $dummy = array(
    'group' => t('amoCRM'),
    'named parameter' => TRUE,
    'base' => 'amocrm_rules_tasks_action',
    'parameter' => array(
      'responsible_user_id' => array(
        'label' => t('Responsible user'),
        'type' => 'integer',
        'optional' => TRUE,
        'options list' => '_amocrm_user_list',
      ),
      'element_id' => array(
        'label' => t('Related entity ID'),
        'description' => t('Contact or lead ID.'),
        'type' => 'integer',
      ),
      'element_type' => array(
        'label' => t('Related entity type'),
        'type' => 'integer',
        'options list' => '_amocrm_rules_related_entity_type_list',
      ),
      'task_type' => array(
        'label' => t('Task type'),
        'type' => 'integer',
        'task_types_list' => $task_types,
        'options list' => '_amocrm_rules_task_types_list',
      ),
      'text' => array(
        'label' => t('Task description'),
        'type' => 'text',
      ),
      'complete_till' => array(
        'label' => t('Complete till'),
        'type' => 'date',
      ),
    ),
  );

  foreach (array('create', 'update') as $action_type) {
    $actions["tasks_$action_type"] = $dummy;
    $actions["tasks_$action_type"]['label'] = t(
      '@action @name in amoCRM',
      array('@action' => t(drupal_ucfirst($action_type)), '@name' => t('task'))
    );
  }

  $actions['tasks_update']['parameter']['update_id'] = array(
    'label' => t('Task to update (ID)'),
    'type' => 'integer',
  );

  return $actions;
}

/**
 * Base action callback for actions related tasks.
 */
function amocrm_rules_tasks_action($args, $element) {
  $name = $element->getElementName();
  $name = explode('_', $name);
  $op = $name[1] == 'update' ? $name[1] : 'add';

  if (empty($args['responsible_user_id'])) {
    if ($args['element_type'] == AMOCRM_API_ENTITY_LEAD) {
      $element_info = amocrm_api_lead_load(amocrm_api_get_default_client(), $args['element_id']);
    }
    elseif ($args['element_type'] == AMOCRM_API_ENTITY_LEAD) {
      $element_info = amocrm_api_contact_load(amocrm_api_get_default_client(), $args['element_id']);
    }
    else {
      throw new Exception('Invalid element type');
    }
    $args['responsible_user_id'] = $element_info['responsible_user_id'];
  }

  $info = array(
    'responsible_user_id' => $args['responsible_user_id'],
    'element_id' => $args['element_id'],
    'element_type' => $args['element_type'],
    'task_type' => $args['task_type'],
    'text' => token_replace($args['text']),
    'complete_till' => $args['complete_till'],
  );

  if ($op == 'update') {
    $info['id'] = $args['update_id'];
  }

  $function = 'amocrm_api_tasks_' . $op;
  $function(amocrm_api_get_default_client(), array($info));
}

/**
 * Provides standard tasks types list.
 */
function _amocrm_rules_task_types_list($element, $name) {
  $info = $element->info();
  if (!empty($info['parameter']['task_type']['task_types_list'])) {
    return $info['parameter']['task_type']['task_types_list'];
  }
  return array();
}

/**
 * Provides entity types list for a form statuses.
 */
function _amocrm_rules_related_entity_type_list() {
  return array(
    AMOCRM_API_ENTITY_CONTACT => t('Contact'),
    AMOCRM_API_ENTITY_LEAD => t('Lead'),
  );
}
